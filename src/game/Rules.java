package game;

import lib.TextIO;

/*Siin klassis on reeglite tekst, mis trükitakse välja
 * vaid siis, kui kasutaja seda soovib.
 * Meetod ei väljasta midagi, kui sisestatakse temasse täisarv vigu, mida mängides teha võib.
 */

public class Rules {

	public static void rules(int a) {
		System.out.println("FOR RULES PRESS X & ENTER!");
		System.out.println("FOR CONTINUE PRESS ANY KEY!");
		String input = TextIO.getlnString();
		if (input.equals("X")) {
			System.out.println("COMPUTER GIVES YOU THE NUMBER OF LETTERS IN YOUR WORD. TRY TO GUESS THEM. ");
			System.out.println("IF YOU GUESS RIGHT, COMPUTER WILL SHOW YOU WHERE THEY LOCATE IN YOUR WORD");
			System.out.println("IF YOUR GUESS IS WRONG, THE MAN YOU HAVE TO SAVE IS ONE STEP CLOSER TO BEING HANGED.");
			System.out.println("SAVE HIM. YOU CAN GUESS WRONG " + a + " TIMES.");
			System.out.println("ALSO SWITCH ON CAPS LOCK WHEN PLAYING.");
			System.out.println();
		}
	}

}

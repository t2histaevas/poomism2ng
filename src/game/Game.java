package game;

import java.util.ArrayList;
import details.List;
import lib.TextIO;

//Siin klassis on main meetod ning selles klassis mäng töötab.

public class Game {

	public static void main(String[] args) {

		boolean playGame; // muutuja deklaratsioon
		playGame = true; // kuni muutuja playGame = true mäng käib

		while (playGame == true) {
			System.out.println("HANGMAN!");

			int a = 5; // muutuja deklaratsioon, selle muutuja väärtus on
						// lubatud vigade arv

			Rules.rules(a); // see meetod tuleb klassist Rules, kui kasutaja
							// tahab reegleid lugeda, vajutab ta "X"

			String[] letters; // muutuja deklaratsioon, letters on Stringide
								// massiiv, mis on sõna, mida kasutaja peab ära
								// arvama
			letters = List.letters(); // paketist details, klassist List tuleb
										// meetod, mis võtab word tüüpi objekti
										// ning teeb sellest Stringide massiivi.

			int length; // length on äraarvatava sõna pikkus
			length = letters.length;

			System.out.println("THERE ARE " + length + " LETTERS IN YOUR WORD.");

			System.out.println();

			/*
			 * Järgnev ArrayListi deklaratsioon, tekib ArrayList, mis sisaldab
			 * sama palju alakriipse, kui on letters massiivis String liikmeid.
			 */
			ArrayList<String> dashes = new ArrayList<String>();
			for (int i = 0; i < length; i++) {
				dashes.add("_");
			}

			// Alakriipsudest koosneva massiivi dashes väljaprintimine.
			for (int i = 0; i < length; i++) {
				System.out.print(dashes.get(i) + " ");
			}

			System.out.println();

			// ArrayListi deklaratsioon. Vastab ArrayList koosneb kasutaja poolt
			// sisestatud Stringidest.
			ArrayList<String> guess = new ArrayList<String>();

			int mistakes; // muutuja mistakes(vigade arv) deklaratsioon
			mistakes = 0;

			int tries; // muutuja tries, mis on kokku pakutud tähtede arv
			tries = 0;

			/*
			 * Järgnev ArrayList koosneb kõigist tähtedest, mida kasutaja võib
			 * pakkuda, täpitähed ja numbrid ning lowercase tähed pole lubatud
			 */
			ArrayList<String> possibleLetters = new ArrayList<String>();
			possibleLetters.add("Q");
			possibleLetters.add("W");
			possibleLetters.add("E");
			possibleLetters.add("R");
			possibleLetters.add("T");
			possibleLetters.add("Y");
			possibleLetters.add("U");
			possibleLetters.add("I");
			possibleLetters.add("O");
			possibleLetters.add("P");
			possibleLetters.add("A");
			possibleLetters.add("S");
			possibleLetters.add("D");
			possibleLetters.add("F");
			possibleLetters.add("G");
			possibleLetters.add("H");
			possibleLetters.add("J");
			possibleLetters.add("K");
			possibleLetters.add("L");
			possibleLetters.add("Z");
			possibleLetters.add("X");
			possibleLetters.add("C");
			possibleLetters.add("V");
			possibleLetters.add("B");
			possibleLetters.add("N");
			possibleLetters.add("M");

			while (mistakes != a) { // mäng kestab seni, kuni kasutaja pole
									// lubatud vigade arvu ületanud

				if (tries == 0) {
					System.out.println("GUESS!");
				}

				if (tries > 0) {
					System.out.println("GUESS AGAIN!");
				}

				guess.add(TextIO.getln()); // lisab pakutud tähtede ArrayListi
											// kasutaja poolt pakutud täha
				System.out.println();

				/*
				 * Kontrollib, kas kasutaja on ikka sisestanud ühekordse
				 * uppercase tähe, mitte numbri ega lowercase tähe.
				 */
				while (!possibleLetters.contains(guess.get(tries))) {
					System.out.println("YOU CAN ONLY GUESS UPPERCASE LETTERS! NOTHING ELSE IS ALLOWED!");
					guess.set(tries, TextIO.getln());
				}

				boolean Control;
				Control = false;

				/*
				 * Järgnev kontrollib, kas pakutud täht on olemas arvuti poolt
				 * valitud sõnas, kui on asendab alakriipsude ArrayListis
				 * vastava alakriipsu selle tähega
				 */
				for (int i = 0; i < length; i++) {
					if (letters[i].equals(guess.get(tries))) {
						dashes.set(i, letters[i]);
						Control = true;
					}
				}

				tries++; // pakutud kordade arv suureneb

				/*
				 * Prindib välja alakriipsude massiivi, mis on muutunud, kui
				 * kasutaja pakkus õige tähe.
				 */
				for (int j = 0; j < length; j++) {
					System.out.print(dashes.get(j) + " ");
				}

				System.out.println();
				System.out.println();

				// Kui kasutaja arvas valesti, suureneb vigade arv ühe võrra
				if (!Control) {
					mistakes++;
				}

				int m = a - mistakes;
				if (m > 0) {
					System.out.println("YOU HAVE " + m + " WRONG GUESSES LEFT!");
				} else if (m ==1) {
					System.out.println("YOU HAVE " + m + " GUESS LEFT!");
				}

				// Prindib välja kõik seni kasutaja poolt pakutud sõnad.
				if (m > 0) {
					System.out.print("YOUR GUESSES:");
					for (int l = 0; l < guess.size(); l++) {
						System.out.print(" ");
						System.out.print(guess.get(l) + " ");
					}
					System.out.println();
				}

				details.Boy.theMan(mistakes); // Võtab details paketist Boy
												// klassist theMan meetodist
												// vastava mehikese kujutise
				System.out.println();

				// Juhul kui mäng on võidetud
				if (GameEnding.gameWon(letters, dashes) == true) {
					System.out.println("YOU WON! HE IS SAFE");
					System.out.println(" \\0/");
					System.out.println("  |");
					System.out.println(" / \\");
					break;
				}
			}

			// Juhul kui mäng on kaotatud
			if (GameEnding.gameLost(mistakes, a) == true) {
				System.out.println("YOU LOST. HE IS DEAD");
				System.out.println("AND GONE!!!");
				System.out.println();
				System.out.print("YOUR WORD WAS: ");
				for (int u = 0; u < letters.length; u++) {
					System.out.print(letters[u]);
				}
				System.out.println();

			}

			// Kui kasutaja tahab veel kord mängida trükib ta sisse "X" ja
			// programm läheb tagasi while loopi algusesse.
			System.out.println();
			System.out.println("PLAY AGAIN? PRESS X & ENTER");
			System.out.println("QUIT? PRESS ANY KEY!");
			String input = TextIO.getln();
			if (input.equals("X")) {
				playGame = true;
			} else {
				playGame = false;
			}
		}
	}
}
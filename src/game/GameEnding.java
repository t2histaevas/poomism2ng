package game;

import java.util.ArrayList;

/* Siin klassis on kaks meetodit, mis vastavalt saadud andmetele
 * otsustavad, kas kasutaja on mängu võitnud või kaotanud.
 */

public class GameEnding {
	
/*Siia meetodisse sisestatkse tehtud vigade arv ning lubatud vigade arv.
 *Kui need on võrdsed väljastab meetod boolean väärtuse true.
 */
	public static boolean gameLost(int mistakes, int a) {

		boolean Control; //muutuja deklaratsioon
		Control = false;

		if (mistakes == a) {
			Control = true;
		} else {
			Control = false;
		}
		return Control;
	}
	
/* Siia meetodisse sisestakse Stringide massiiv ning Stringide ArrayList.
 * Meetod kontrollib, kas mõlema massiivi vastavad liikmed on samad.
 * Kui massiivide vastavad liikmed klapivad, väljastab
 * meetod boolean tüüpi väärtuse true.
 */

	public static boolean gameWon(String[] letters, ArrayList<String> dashes) {

		boolean Control; //muutuja deklaratsioon
		Control = true;

		for (int i = 0; i < letters.length; i++) {
				if (!letters[i].equals(dashes.get(i))){
					Control = false;
					break;
				}
			}

		return Control;
	}
}
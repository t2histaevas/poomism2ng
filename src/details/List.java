package details;

import details.Word;
import java.util.ArrayList;

public class List {
	/*
	 * Järgnev meetod võtab objekti word(ehk valitud sõna) Word klassist meetodi
	 * change ning väljastab valitud sõna Stringide massiivina.
	 */
	public static String[] letters() {
		Word word = choose();
		String[] letters = word.change();
		return letters;
	}

	/*
	 * Järgnev meetod sisaldab ArrayListi, milles on kakskümmend sõna, millest
	 * Arvuti võtab suvaliselt ühe. See meetod väljastab valitud sõna.
	 */

	public static Word choose() {
		ArrayList<Word> wordList = new ArrayList<Word>();
		wordList.add(new Word("OCCASIONALLY"));
		wordList.add(new Word("GRANDMOTHER"));
		wordList.add(new Word("INDEPENDENT"));
		wordList.add(new Word("TOBACCO"));
		wordList.add(new Word("MYSTERIOUS"));
		wordList.add(new Word("RHYME"));
		wordList.add(new Word("EXPLANATION"));
		wordList.add(new Word("INFINITY"));
		wordList.add(new Word("EXISTENCE"));
		wordList.add(new Word("MAGNIFICENT"));
		wordList.add(new Word("GREYHOUND"));
		wordList.add(new Word("CENTIPEDE"));
		wordList.add(new Word("SINISTER"));
		wordList.add(new Word("ENCOUNTER"));
		wordList.add(new Word("LANGUAGE"));
		wordList.add(new Word("DINOSAUR"));
		wordList.add(new Word("MEDITERRANEAN"));
		wordList.add(new Word("YOUTUBER"));
		wordList.add(new Word("GRANDFATHER"));
		wordList.add(new Word("SEASIDE"));

		Word choice;
		choice = wordList.get((int) (Math.random() * 20));

		return choice;

	}

}

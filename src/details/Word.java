package details;

/*See siin on konstruktormeetod objekti Word jaoks.
 *Kõik selles mängus ära arvatavad sõnad on algselt Word tüüpi objektid.
 */

public class Word {

	private String word;

	public Word(String word) {
		this.word = word;
	}

	// Järgnevasse meetodisse sisestatkse objekt word, ning ta väljastab Stringide massiivi.
	public String[] change() {
		String[] letters = word.split("");
		return letters;
	}

	/*
	 * See meetod ütleb, et kui tahta word välja printida, siis ta ei prindi
	 * välja wordi asukohta vaid selle väärtuse. Kirjutab üle toString meetodi.
	 */
	@Override
	public String toString() {
		return word;
	}

}

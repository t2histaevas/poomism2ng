package details;

public class Boy {
	
/*Meetodisse theMan sisestakse täisarv vigu
 *ning nende põhjal prindib see välja vastava teksti. Meetod ei väljasta midagi
 */
	
	public static void theMan(int mistakes) {
		if (mistakes == 0) {
			System.out.println("__________");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
		}
		if (mistakes == 1) {
			System.out.println("__________");
			System.out.println("|        |");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
		}
		if (mistakes == 2) {
			System.out.println("__________");
			System.out.println("|        |");
			System.out.println("|        0");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
		}
		if (mistakes == 3) {
			System.out.println("__________");
			System.out.println("|        |");
			System.out.println("|        0");
			System.out.println("|        |");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
		}
		if (mistakes == 4) {
			System.out.println("__________");
			System.out.println("|        |");
			System.out.println("|        0");
			System.out.println("|       /|\\");
			System.out.println("|         ");
			System.out.println("|         ");
			System.out.println("|         ");
		}
		if (mistakes == 5) {
			System.out.println("__________");
			System.out.println("|        |");
			System.out.println("|        0");
			System.out.println("|       /|\\");
			System.out.println("|       / \\");
			System.out.println("|         ");
			System.out.println("|         ");

		}
	}
}